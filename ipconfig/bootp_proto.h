/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef IPCONFIG_BOOTP_PROTO_H
#define IPCONFIG_BOOTP_PROTO_H

int bootp_send_request(struct netdev *dev);
int bootp_recv_reply(struct netdev *dev);
int bootp_parse(struct netdev *dev, struct bootp_hdr *hdr, uint8_t * exts,
		int extlen);
int bootp_init_if(struct netdev *dev);

#endif /* IPCONFIG_BOOTP_PROTO_H */
