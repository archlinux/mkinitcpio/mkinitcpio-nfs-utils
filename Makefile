
PROGRAMS = ipconfig nfsmount
CLEAN_PROGRAMS = $(addsuffix -clean, $(PROGRAMS))
INSTALL_PROGRAMS = $(addsuffix -install, $(PROGRAMS))

DESTDIR	?=
prefix  ?= /lib/initcpio
bindir  ?= $(prefix)


.PHONY: all
all: $(PROGRAMS)

.PHONY: $(PROGRAMS)
$(PROGRAMS):
	$(MAKE) -C $@

.PHONY: clean
clean: $(CLEAN_PROGRAMS)

.PHONY: $(CLEAN_PROGRAMS)
$(CLEAN_PROGRAMS):
	$(MAKE) -C $(subst -clean, ,$@) clean

.PHONY: install
install: $(INSTALL_PROGRAMS)

.PHONY: $(INSTALL_PROGRAMS)
$(INSTALL_PROGRAMS):
	$(MAKE) -C $(subst -install, ,$@) install
